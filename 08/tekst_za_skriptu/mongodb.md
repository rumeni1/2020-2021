# MongoDB baza podataka

Okruženje Node.js podržava mogućnost rada sa velikim brojem popularnih baza podataka. Neke od njih su MySQL, PostgreSQL, Redis, SQLight i MongoDB. U ovoj sekciji upoznaćemo MongoDB bazu i naučiti kako možemo da unosimo, pretražujemo, ažuriramo i brišemo podatke.

## MongoDB baza podataka

MongoDB baze podataka se ubrajaju u grupu nerelacionih baza podataka zasnovanih na dokumentima (engl. document oriented databases). Za razliku od relacionih baza u kojima je osnovni koncept _relacija_, ovu grupu baza karakteriše koncept _dokumenta_. Dokumenti su strukture koji sadrže parove svojstava i vrednosti i po formatu podsećaju na JSON objekte. Osnovna motivacija za ovakvu organizaciju je pojednostavljenje razvoja i unapređivanje skalabilnosti pa su i sheme koje baze ovog tipa koriste proširive i ne tako striktne kao kod relacionih baza. Takođe, zbog same organizacije dokumenata, postignuto je zaobilaženje kompleksnih upita spajanja što značajno može da doprinese efikasnosti.

Kao što smo napomenuli, dokument se sastoji od parova imena i vrednosti. Imena odgovaraju svojstvima entiteta koje želimo da pratimo. Po tipu podataka su to niske i u njihovom izboru postoji nekoliko restrikcija. Na primer, ime `_id` je rezervisano za primarni ključ dokumenta, ne može se uvek koristiti `$` karakter na početku imena (videćemo zašto) i ne može se uključiti _null_ karakter. Vrednosti objekata mogu biti izražene primitivnim MongoDB tipovima kao što su niske, numeričke vrednosti, logičke vrednosti, binarne reprezentacije objekata, nizovi ovih vrednosti, drugi dokumenti i mnogi drugi. U nastavku možete videti primer jednog dokumenta.

```javascript
{
    username: 'bane',
    email: 'bane@gmail.com',
    password: 'bane12345',
    birth: new Date('Jun 07, 1994'),
    contributions: 34,
    programmingSkills: ['JavaScript', 'Python'],
    status: 'active'
}
```

Za pristup svojstvima na nivou dokumenata koristi se `tačka notacija` (engl. dot notation).

Grupe MongoDB dokumenata se zovu `kolekcije`. Kolekcijama u kontekstu relacionih baza podataka odgovaraju tabele. Dokumenti koji se nalaze u istoj kolekciji ne moraju imati istu shemu, tj. ne moraju imati isti skup svojstava ili vrednosti istog tipa. Ukoliko ovo nije poželjno svojstvo, MongoDB ostavlja mogućnost insistiranja na validacionim pravilima prilikom kreiranja novih dokumenata ili upisa i izmena u postojeće.  
![Prikaz MongoDB kolekcije](./assets/mongo_collection.png)

Na fizičkom nivou dokumenti se čuvaju u BJSON formatu, binarizovanom JSON formatu.

## Instalacija i pokretanje

Sve smernice za instalaciju, usklađene sa različitim verzijama sistema i programa, je najbolje potražiti na [zvaničnom sajtu](https://docs.mongodb.com/manual/installation/) zajednice. Verzija koju prati ovaj dokument je _MongoDB 4.2 Community Edition_.

Rad sa MongoDB bazom podataka moguće je u nekoliko različitih modaliteta. Pre svega, moguće je lokalno interagovati sa bazom kroz takozvani _mongo shell_ terminal. Nakon što se instalira i u skladu sa instrukcijama pokrene `mongod` MongoDB _deamon_ proces, _mongo shell_ je moguće aktivirati unošenjem komande `mongo` u sistemski terminal. Nakon ovoga će se pojaviti pozdravni prompt, a dalju interakciju sa bazom je moguće realizovati unošenjem upita i konfiguracionih naredbi. Tekuća sesija se obično prekida izvršavanjem funkcije `quit()`.

Nešto udobniji rad sa samom bazom preko grafički dopadljivog i intuitivno organizovanog interfejsa moguće je uz korišćenje alata [Compass](https://www.mongodb.com/products/compass). Sve radnje, od povezivanja sa bazom podataka, kreiranja nove kolekcije i novih unosa do pretraživanja i vizualizacije, realizuju se kroz prozore i odabire njihovih opcija što značajno može ubrzati i pojednostaviti rad.

Funkcionalnost MondoDB baze podataka dostupna je i u _oblaku_ preko sistema koji se zove [Atlas](https://www.mongodb.com/cloud/atlas). Ovaj servis omogućava stalnu dostupnost, kompatibilnost sa svim pratećim bibliotekama, skalabilnost i mnoge druge pogodnosti koje su važne za uspešno i kontinuirano poslovanje.

Ovde ćemo pomenuti i mogućnost programskog rada sa MongoDB bazom podataka kroz sisteme koji mapiraju objekte u dokumente (engl. object-document mappers, skraćeno ODM). Zahvaljujući ovom konceptu, omogućeno je interagovanje sa bazom kroz objekte i njihove konfiguracije, rasterećeno smantičkih osobenosti baze i njenih upitnih jezika. Ova unifikacija je programerima posebno važna u istovremenom radi sa više različitih baza podataka. Zajednica nudi nekoliko opcija u radu sa MongoDB bazama poput [Waterline](https://waterlinejs.org/), [Objection](https://vincit.github.io/objection.js/), [Sequelize](https://sequelize.org/), [GraphQL](https://graphql.org/), a mi ćemo se u daljem radu, nakon što naučimo osnove konstrukcije za rad sa MongoDB bazom, opredeliti za [mongoose](https://mongoosejs.com/).

## MongoDB Shell

Izvršavanjem podržanih komandi i pozivom odgovarajućih MongoDB metoda u _mongo shell_ terminalu moguće je relizovati sve funkcionalnosti nad bazom podataka.

Komandom `show dbs` se mogu izlistati nazivi svih raspoloživih baza. Nakon inicijalnog pokretanja, mogu se videti `test`, `admin`, `config` i `local` baza podataka. Naredbnom `use ime_baze` se pristupa bazi sa nazivom koji se zadaje. Ukoliko odabrana baza ne postoji, MongoDB sistem za upravljanje bazom podataka će je kreirati. Svi upiti koji se nadalje budu zadavali izvršavaće se nad ovako odabranom bazom sve dok se naredba ne ponovi sa nazivom druge baze. Podrazumevana baza za rad se zove `test`. Naredbom `db` uvek može pročitati ime tekuće baze.

Skup svih kolekcija jedne baze može se dobiti komandom `show tables`. Da bi se kreirala nova kolekcija podataka dovoljno je kreirati prvi dokument u njoj ili pozvati `createCollection` metodu. Prvi pristup je u praksi zastupljeni, dok se drugi koristi u specijalnim slučajevima kada je potrebno kreirati kolekcije sa validacijama ili kolekcije fiksnih dimenzija.

Upis jednog ili više dokumenata u kolekciju se vrši pozivom metode `insert` koja očekuje sadržaj dokumenata koje treba pridružiti kolekciji. Tako će sledeći poziv kreirati kolekciju `users` i dodati joj dokument sa opisom prvog korisnika.

```javascript
> db.users.insert({
... username: 'bane',
... email: 'bane@gmail.com',
... password: 'bane123',
... status: 'active',
... contributions: 34,
... programmingSkills: ['JS', 'Python']
... })
WriteResult({ "nInserted" : 1 })
>
```

Svaki kreirani dokument u kolekciji dobija jedinstveni identifikator koji se čuva u svojstvu `_id` dokumenta. U pitanju je reprezentacija duga 12 bajtova. Funkcija `insert` nas nakon izvršavanja obaveštava o broju uspešnog kreiranih dokumenata. Za dodavanje novih dokumenata kolekciji mogu se koristiti i funkcije `insertOne` i `insertMany`. Kao što njihova imena govore, `insertOne` se koristi za dodavanje tačno jednog dokumenta, dok funkcija `insertMany` omogućava istovremeno dodavanje većeg broja dokumenata.

Za čitanje i pretraživanje kolekcije koristi se funkcija `find`. Ova funkcija očekuje objekat kojim se specificiraju svojstva koja traženi dokumenti treba da zadovolje. Pozivom `find({})` se izlistavaju svi dokumenti odabrane kolekcije. Za izlistavanje dokumenata kolekcije čija svojstva imaju neku konkretnu vrednost (poređenje po jednakosti) se može relizovati pozivom funkcije `find` na način `find({ime_polja: vrednost})`. Na primer, pozivom `find({status: 'active'})` mogu se dobiti informacije o korisnicima koji su aktivni, a pozivom pozivom `find({programmingSkills: 'JS'})` informacije o korisnicima koji u nizu veština imaju naveden _'JS'_. Ukoliko se navede više ovakvih parova poređenja, podrazumeva se njihova konjunkcija. Na primer, upitom `find({status: 'active', programmingSkills: 'JS'})` se pronalaze svi aktivni korisnici koji poznaju 'JS'.

Funkcija `find` za pronađene dokumente podrazumevano vraća sva svojstva. Ukoliko je potrebno promeniti ovo ponašanje, kao drugi argument funkcije `find` se može navesti objekat `projekcije` sa svojstvima koja odgovaraju svojstvima dokumenata i vrednostima `1` ili `0` u zavisnosti od toga da li ih treba uključiti u prikazu rezultata ili ne. Na primer, pretraga `find({status: 'active'}, {'password': 0})` neće uključiti polje sa šifrom u prikazu rezultata.

Za kompeksinije upite u kojima je potrebno vršiti ne samo poređenja po jednakosti mogu se koristiti `operatori upita` (engl. query operators). Neki od njih su `$lt` i `$lte` (sa značenjem _less than_ tj. _less than or equal_), `$gt` i `$gte` (sa značenjem _greater than_ tj. _greater than or equal_), `$in` i `$nin` (sa značenjem _not in_). Očekivani način zadavanja ovih operatora je u formi koja je navedena niže.

```code
{
  ime_polja: {
    operator: vrednost
  },
  ...
}
```

Na primer, zapis `{contributions: {$gt: 30}}` odgovara konstrukciji `contributions> 30`, a zapis `{contributions: {$gt: 30, $lt: 100}}` konstrukciji `contributions> 30 AND contributions< 100`. Slično se zapisom `{programmingSkills: {$in: ['JS', 'C']}}` mogu pronaći dokumenti u kojima je u nizu veština prisutna neka od niski _'JS'_ ili _'C'_.

Ukoliko je delove upita potrebno vezati disjunkcijom ili ih negirati, na raspolaganju su nam `logički operatori` tipa `$and`, `$or`, `$not` i `$nor`. Očekivani način zapisivanja koji koriste ove operatore je

```code
{
  operator: [
    { svojstvo: vrednost },
    { svojstvo: vrednost },
    ...
  ]
}
```

Na primer, upit kojim se izdvajaju svi korisnici koji su aktivni ili koji imaju više od 15 doprinosa, može se zapisati navođenjem objekta filtriranja

```javascript
{
  $or: [{ status: 'active' }, { contributions: { $gt: 15 } }];
}
```

U prethodnim upitima vrednosti tekstualnih polja su poređenja sa niskama onakve kakve su zadate. Nekada je potrebno proveriti da li vrednost polja počinje ili se završava nekom niskom, ili da li sadrži neku nisku. U ovim slučajevima možemo koristiti konstrukcije koje po obliku podsećaju na regularne izraze, pa navođenjem niske između karaktera `/^` i `/`zahtevamo da polje počinje nekom niskom, navođenjem niske između `/` i `$/` da se polje završava nekom niskom, a navođenjem niske između `/` i `/` da niska sadrži zadatu podnisku. Na primer, sve korisnike čija se email adresa završava sa _@gmail.com_ možemo dobiti sledećim blokom koda.

```javascript
db.users.find({email: /@gmail.com$/)}
```

Poziv funkcije `find` se obično kombinuje se funkcijom `pretty` kako bi se dobio lepši i pregledniji ispis informacija.
Ukoliko je prilikom prikaza pronađenih dokumenata potrebno ograničiti se na nekoliko prvih, može se koristiti funkcija `limit`.
Sledećim kodom se izlistavju informacije o prva tri aktivna korisnika.

```javascript
db.users.find({active: 'status')}.limit(3)
```

Ažuriranje kolekcije se vrši funkcijama `updateOne` i `updateMany`, u zavisnosti od broja dokumenata koje treba ažurirati. Privim argumentom-upitom se vrši odabir dokumenata za ažuriranje, dok se drugim navodi objekat sa izmenama. Polja koja treba promeniti su praćena `$set` operatorom, a forma objekta sa izmenama je sledeća

```code
{
  operator: { svojstvo: vrednost, ... },
  operator: { svojstvo: vrednost, ... },
  ...
}
```

Na primer, upit kojim se menja šifra korisnika sa korisničkim imenom _bane_ na vrednost _123456_ je

```javascript
db.users.updateOne(
  {
    username: 'bane',
  },
  {
    $set: { password: '123456' },
  }
);
```

Ukoliko polje koje treba promeniti ne postoji u pronađenim dokumentima, podrazumevano će biti kreirano. Takođe, funkcije ove grupe dozvoljavaju i navođenje trećeg po redu konfiguracionog objekta. Tako, ukoliko se navede konfiguracija `{upsert: true}`, u slučaju da objekat nad kojim treba izvršiti ažuriranje ne postoji biće inicirano i njegovo kreiranje.

U operatore izmene ubrajaju se i operatori `$unset` kojima se briše svojstvo objekta, `$inc` i `$dec` kojima se vrednost polja može uvećati tj. smanjiti za zadatu vrednosti, `$currentDate` kojim se vrednost polja postavlja na tekući vremenski trenutak i mnogi drugi.

Na primer, upit kojim se povećava doprinos korisnika sa korisničkim imenom _bane_ za _5_ može se zapisati sledećim blokom koda.

```javascript
db.users.updateOne(
  {
    username: 'bane',
  },
  {
    $inc: { contributions: 5 },
  }
);
```

Upit kojim se briše svojstvo _programmingSkills_ se može zapisati sledećim blokom koda.

```javascript
db.users.updateOne(
  {
    username: 'bane',
  },
  {
    $unset: { programmingSkills: '' },
  }
);
```

Upit kojim se postavlja polje _lastLogin_ na tekući vremenski trenutak u formi vremenskog žiga. Alternativa je forma ISO datuma koja se zahteva kroz vrednost `$type` svojstva postavljenu na _"date"_.

```javascript
db.users.updateOne(
  {
    username: 'bane',
  },
  {
    $currentData: { lastLogin: { $type: 'timestamp' } },
  }
);
```

Brisanje dokumenata kolekcije, takođe u zavisnosti od broja dokumenata koje treba obrisati, vrši se pozivom funkcija `deleteOne` ili `deleteMany`. Na primer, pozivom, `db.users.deleteOne({_id: "5eb2937c1ce5a5aee688581a"})` se briše objekat sa zadatim identifikatorom. Brisanje kolekcije i svih dokumenata koji se nalaze u njoj se može postići funkcijom `drop`. Tako se pozivom `db.users.drop()` može obrisati cela kolekcija `users`. Brisanje same baze podataka i svih dokumenata i kolekcija koje se nalaze u njoj moguće je uz prethodno pozicioniranj funkcijom `dropDatabase`.

## Mongo

Uz instalaciju MongdoDB baze podataka, osim pomentutog _mongo shell_ alata, instaliraju se i alati koji omogućavaju druge korisne funkcionalnosti poput učitavanja i izvoza podataka, dijagnostike i konfiguracija.

Alat `mongoimport` omogućava uvoz podataka u željenu kolekciju odabrane baze. Na primer, komandom `mongoimport --db shop --collection products --file products.json` mogu se uvesti svi proizvodi navedeni u `products.json` datoteci u kolekciju sa imenom `products` baze `shop`. Podržani format datoteka je Extended JSON, CSV i TSV.

Alat `mongoexport` se koristi za izvoz podataka. Na primer, komandom `mongoexport --db shop --collection products --out products.json` se mogu izvesti svi podaci sadržani u kolekciji `products` baze `shop`. Podrazumevani format izvoza je JSON. Navođenjem opcije `--type` se tip može promeniti na CSV. Postoje i druge korisne opcije poput `--fields` kojom se mogu navesti imena polja koja prilikom izvoza treba uzeti u obzir ili opcije `--pretty` kojom se utiče na pregledan format izvoza.

## Mongoose

[Mongoose](https://mongoosejs.com/) je Node.js biblioteka koja omogućava preslikavanje kolekcija i dokumenata baze u objekte i programsku manipulaciju nad njima.

Mongoose se može instalirati poput drugih Node.js paketa naredbom `npm install mongoose` i na nivou fajlova u kojima se koristi učitati pozivom funkcije `require`.

Prvi prirodan korak u daljem radu je svakako povezivanje sa bazom podataka. Konekcija se može uspostaviti pozivom funkcije `connect` koja očekuje kao argument mrežno ime servera. Podrazumevano je za MongoBD konekcije otvoren port `27017`. Sledeći kod ilustruje povezivanje sa lokalnom bazom _demo_.

```javascript
mongoose.connect('mongodb://localhost:27017/demo', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
```

Funkciji `connect` je prosleđen i konfiguracioni objekat sa svojstvima `userNewUrlParser` i `useUnifiedTopology` sa ciljem da se premoste prelazi između različiti verzija biblioteke. Među ovim podešavanjima se mogu naći i korisničko ime i šifra ukoliko to sistem za upravljanje bazom podataka zahteva i mnoga podešavanja niskog nivoa koja se odnose na rad sa soketima i vremenske sinhronizacije.

Prilikom povezivanja sa bazom do greške može doći odmah po iniciranju konekcije ili nešto kasnije. Greške prvog tipa se moraju hvatati dodavanjem `try-catch` bloka.

```javascript
try {
  await mongoose.connect('.....');
} catch (error) {
  // obrada greške;
}
```

Greške drugog tipa se pokrivaju reagovanjem na `error` događaj blokom koda

```javascript
mongoose.connection.on('error', (error) => {
  // obrada greške
});
```

Nakon povezivanja sa MongoDB bazom, potrebno je kreirati `sheme`. Jedna shema mapira jednu MongoDB kolekciju i naglašava koja svojstva i kod tipa imaju dokumenti koji joj pripradaj. Shema koja odgovara našoj kolekciji `users` bi se mogla uvesti na sledeći način.

```javascript
const usersSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'active',
  },
  age: Number,
  programmingSkills: [String],
});
```

Shema se kreira korišćenjem konstruktorske funkcije `Schema`. Objekat koji joj se prosleđuje kao argument sadrži popis svojstava i njihovih konfiguracija. Za svako svojstvo očekuje se navođenje tip opisanog `ShemaType` tipom. Raspoloživi tipovi su `String`, `Number`, `Boolean`, `Date`, `Buffer`, `Mixed`, `Array`, `ObjectId`, `Map`, `Decimal128` i mogu se navesti direktno, nakon imena svojstva, ili u okviru konfiguracionog objekta svojstvom `type`. U konfiguracionim objektima je uz navođenje tipova, moguće navesti i druga korisna podešavanja i validacije od kojih neka zavise od tipa, a neka su univerzalna.

Svim svojstvima je moguće pridružiti podrazumevane vrednosti podešavanjem `default`, kao što je to u slučaju `status` svojstva naše sheme. Podešavanjem `enum` se može navesti niz sa dozvoljenim vrednostima svojstva. Takođe, za sva polja je moguće naglasiti da li su obavezna ili ne navođenjem podešavanja `required` sa logičkom vrednošću. Za stringove je, recimo, preko podešavanja `lowercase` ili `upercase` sa logičkim vrednostima moguće naglasiti da li vrednosti svojstava treba transformisati u mala ili velika slova, a može se naglasiti koja im je `maksimalna` ili `minimalna` dužina podešavanjima `maxlength` i `minlength`. Za numerička svojstva je moguće naglasiti minimalnu i maksimalnu vrednost, redom, podešavanjima `min` i `max`.

Ovako uvedene sheme definišu strukturu dokumenata koji će se naći u kolekciji kojoj se shema pridruži. Ukoliko prilikom dodavanja dokumenata ili njihove izmene dođe do nepoštovanja neke od navedenih restrikcija sheme, doći će do greške.

Zgodno je naglasiti i da su sheme dinamičkog tipa i da je naknadno dodavanje svojstava moguće korišćenjem funkcije `add`.

Definicija sheme se transformiše u takozvani model pozivom funkcije `model`. Prvi argument ove funkcije označava ime kolekcije za koju se shema veže, dok drugi argument predstavlja samu shemu. Nakon ovog poziva sva interakcija sa bazom podataka je moguća preko rezultujućeg objekta.

```javascript
const usersModel = mongoose.model('users', usersSchema);
```

Mogoose model raspolaže odgovarajućom funkcijom za svaku od radnji nad bazom podataka. Mi ćemo se upoznati sa funkcijama koje prate CRUD operacije, ali je skup funkcija daleko bogatiji i omogućava i rad sa indeksima, virtuelnim svojstvima (svojstvima koja se mogu pridružiti modelu, ali koja neće biti sačuvana u bazi), konfigurisanje samog modela i drugo.

Pre nego li upoznamo funkcije koje će podržati CRUD operacije, prokomentarisaćemo i jedno njihovo zajedničko svojstvo. Naime, sve ove funkcije su asinhronog tipa i mogu se koristiti na jedan od dva načina. Prvi način podrazumeva korišćenje funkcije sa povratim pozivom potpisa `(error, result)=>{ .... }` na mesto poslednjeg argumenta. Objekat `error` predstavlja objekat greške i ukoliko je njegova vrednost `null`, funkcija je uspešno izvršena. U tom slučaju se dalje može raditi i sa njenim rezultatom koji se očekuje preko argumenta `result` i čija vrednost zavisi od vrste upita koja se izvršava. Ako dođe do greške, izvršavanje funkcije se prekida prijavljivanjem izuzetka tipa greške ili pozivanjem rutine za njihovu obradu. Jedna takva konstrukcija je u nastavku.

```javascript
usersModel.findOne({ username: 'maja' }, (error, result) => {
  if (err) {
    // obrada greške
    throw error;
  }
  // obrada rezultata
  console.log(result);
});
```

Drugi način izvršavanja funkcija se zasniva na eksplicitnom pozivu funkcije `exec` za izvršavanje upita koja kao rezultat vraća nativni JavaScript objekat `Promise`. Ova forma izvršavanja zato dozvoljava rad sa `async/await` konstrukcijama.

```javascript
const printUserData = async function (username) {
  const user = await usersModel.findOne({ username: username }).exec();
  console.log(user);
};
printUserData('maja');
```

Biblioteka `mongoose` omogućava kroz objekte tipa `Query` kreiranja upita koji ne moraju da prate nužno sintaksu MongoDB upitnog jezika koju smo uveli u priči o _mongo shell_ terminalu. Tako se, na primer, prethodni upit može zapisati i kao
```javascript
usersModel
  .findOne()
  .where('username')
  .equals(username);
```
uz korišćenje `where` i `equals` funkcija `Query` objekta. Slično, upitu

```javascript
usersModel.
  find({
    email: /@gmail.com$/,
    age: { $gt: 10, $lt: 40 },
    status: 'active
  });
```

odgovara upit

```javascript
usersModel
  .find({ email: /@gmail.com$/ })
  .where('age')
  .gt(17)
  .lt(66)
  .where('status')
  .equals('active');
```
Pored funkcija čija imena odgovaraju imenima operatora pretrage i logičkih operatora, na raspolaganju su i funkcija za obradu rezultata pretrage. Na primer, korišćenjem funkcije `sort` mogu se sortirati rezultujući objekti po željenom svojstvu i poretku, funkcijom `skip` preskočiti neki od njih, a funkcijom `limit` redukovati njihov broj. Na primer, sledećim upitom se prvo sortiraju korisnici rastuće na osnovu korisničkog imena, zatim se prvih 5 rezultata preskače i izdvaja narednih 10 rezultata. 
```javascript
usersModel.find({}).sort({'username': 1}).skip(5).limit(10);
```
Poredak sortiranja je određen numeričkom vrednošću koja se pridružuje svojstvu. Vrednost `1` ukazuje na rastući, a vrednost `-1` na opadajući poredak. Nravno, prilikom sortiranje je moguće kombinovati više uslova.

```javascript
usersModel.find({}).sort({'contributions': 1, 'username': 1});
```

Detaljan popis svih funkcija vezanih za upite može se pronaći u [zvaničnoj dokumentaciji](https://mongoosejs.com/docs/api/query.html).

Kroz ove primere smo indirekto upoznali i funkcije modela za pretraživanje `find` i `findOne`. Ove funkcije očekuju objekat sa svojstvima za pretragu. U radu može biti od koristi i funkcija iz ove grupe sa imenom `findById` koja pronalazi dokument za zadatim identifikatorom `_id`.
```javascript
let id = ObjectId('5eb2c28344c5e14d11dd3205');
const user = await usersModel.findById(id).exec();
```

Dalje ćemo nastaviti sa upoznavanjem funkcija za dodavanje novih dokumenata kolekciji.

Instance modela predstavljaju dokumente. Na primer, na sledeći način možemo kreirati novog korisnika.

```javascript
let newUser = new usersModel({
  _id: mongoose.Types.ObjectId(),
  username: 'Marko',
  password: 'Marko123',
  email: 'marko@gmail.com',
  age: 30,
});
```

Prilikom kreiranja novog dokumenta treba zadati i njegov identifikator `_id`. Njega možemo kreirati pozivom funkcije `ObjectId` koja će generisati identifikator u potrebnoj formi. Ovako kreirani dokumenti se trajno čuvaju pozivom funkcije `save`. Tako će se narednom naredbom dodati još jedan korisnik bazi.

```javascript
await newUser.save();
```

Ažuriranje dokumenta je moguće funkcijama `update`, `updateOne` i `updateMany` ili kombinacijama funkcija za pronalaženje i čuvanje, kao što je opisano u prethodnom primeru. Na primer, promena šifre korisnika čiji je identifikator `userId` poznat se može realizovati nekim od sledećih blokova koda.

```javascript
await userModel.updateOne({ _id: userId }, { $set: { password: '12345' } }).exec();
```

```javascript
const user = await usersModel.findById(id).exec();
user.password = '12345';
await user.save();
```

Brisanje dokumenata kolekcije je moguće funkcijama `deleteOne` i `deleteMany` u zavisnosti od toga da li je potrebno obrisati jedan ili više dokumenata. Sledećim blokom koda se mogu obrisati svi neaktivni korisnici.

```javascript
userModel.deleteMany({ status: { $neq: 'active' } }).exec();
```

U radu mogu biti od koristi i funkcije `countDocuments` i `distinct`. Funkcijom `countDocuments` se prebrojava broj dokumenata koji zadovoljavaju navedene uslove, a funkcijom `distinct` se mogu dobiti jedinstvene vrednosti nekog svojstva. Naredni primeri demonstriraju njihovo korišćenje. Prvim primerom se pronalazi broj aktivnih korisnika, a drugim skup jedinstvenih email adresa.

```javascript
const numberOfActiveUsers = usersModel.countDocuments({ status: 'active' });
const distinctEmails = usersModel.distinct('email');
```

## REST API sa manipulacijom podataka koji se očitavaju iz baze

Sada kada smo upoznali CRUD operacije `mongoose` biblioteke, možemo da unapredimo prethodnu verziju servera koju smo razvili u sekciji o radnom okviru `Express.js` tako što ćemo sve radnje realizovati nad bazom podataka. Modifikaciju ćemo izvršiti i na organizacionom nivou tako da jasno možemo da razdvojimo zaduženja koja pojedinjeni delovi aplikacije imaju.

`Ruter` komponenta aplikacije biće zadužena, kao i do sada, za rutiranje zahteva klijenata. Na osnovu prepoznatih metoda i putanja sadržnih u zahtevu, ruter će sada svu logiku aplikacije prepuštati kontrolerima. `Kontroleri` su komponente aplikacije koje će biti zadužene za obradu prosleđenih podataka i generisanje rezultata. Ukoliko su kontrolerima u radu potrebni podaci oni će se obraćati modelima. `Modelima` se nazivaju komponente aplikacije koje upravljaju podacima (zbog toga nije ni slučajan izbor za imena metode `mongoose` biblioteke za pridruživanje sheme kolekciji). Modeli imaju pun uvid u strukturu baza sa kojima rade i mogućnost da izvrše sve potrebne upite nad njima. 
![Arhitektura aplikacije](./assets/app_architecture.png)

Zbog prethodno opisanog, u novoj verziji aplikacije imaćemo uz `routes` direktorijum i direktorijum koji se zove `models` u kojem ćemo smestiti model i direktorijum `controllers` u kojem ćemo smestiti funkcije kontorolera.

Fajl sa imenom `usersModel.js` će sadržati shemu kolekcije korisnika. Njega ćemo sačuvati u `models` direktorijumu.

```javascript
const mongoose = require('mongoose');
const usersSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'active',
  },
});
const usersModel = mongoose.model('users', usersSchema);
```

Nakon uključivanja `mongoose` biblioteke i definisanja sheme, izvešćemo model tako da drugi delovi aplikacije mogu da ga koriste.

Fajl sa imenom `usersController.js` ćemo sačuvati u `controllers` direktorijumu. Kao što smo najavili kontroleri će komunicirati sa modelom pa ćemo uključiti kreirani model na početku ovog fajla.

```javascript
const User = require('../models/usersModel.js');
```
Na nivou modula odeređenog fajlom `usersController.js` definisaćemo posebne funkcije koje ćemo uparivati sa zahtevima rutera. Detaljno ćemo prokomentarisati način rada funkcije `getUsers` kojom ćemo pročitati informacije o svim korisnicima, a sva ponašanja se prenose i na preostale funkcije kontrolera.

Funkcija `getUsers` kao argumente očekuje objekat `req` koji predstavlja zahtev klijenta, objekat `res` koji predstavlja odgovor koji treba proslediti klijentu i funkciju `next` kojom se prepušta upravljanje sledećoj funkciji srednjeg sloja. Informacije o zahtevu klijenata i pripremljen objekat odgovora kontroler će dobiti od rutera.

```javascript
module.exports.getUsers = async (req, res, next) => {
  try {
    const user = await User.find({}).exec();
    res.status(200).json(user);
  } catch (err) {
    next(err);
  }
};
```

Funkcija `getUsers` poziva funkciju `find` učitanog modela `User` i izvršava je pozivom funkcije `exec`. Pošto ćemo na ovaj način dobiti `Promise` objekat njenom pozivu prethodi `await` naznaka, a samoj funkciji `async` naznaka. Logika funkcije je po prispeću podataka o korisniku ostala ista tj. postavlja se odgovarajući statusni kod i u telo odgovora upisuju podaci o pročitanim korisnicima u JSON formatu. Da bismo ispratili greške koje mogu nastati prilikom ove obrade, dodajemo `try-catch` blok u kojem greške koje se jave prosleđujemo funkcijom `next` sledećoj funkciji srednjeg sloja. To će u našem slučaju biti funkcija definisana na nivou aplikacije sledećim blokom koda.

```javascript
app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  res.status(statusCode).json({
    error: {
      message: err.message,
      status: statusCode,
      stack: err.stack,
    },
  });
});
```

Pošto ćemo ovu funkciju koristiti za obradu svih grešaka koje nastanu u aplikaciji, prvo ćemo proveriti da li je na nivou objekta greške postavljen odgovarajući statusni kod. Kada to nije slučaj, postavićemo statusni kod `500` u duhu REST arhitekturalnih smernica. Ostatkom koda funkcije za obradu greške, postavljamo određeni statusni kod odgovora, a zatim i prosleđujemo u JSON formatu informacije o grešci: njenu poruku i stek poziva koja je greška generisala.

Na nivou ove funkcije možemo, ukoliko je potrebna preciznija analiza, analirati i sam tipe greške. Na primer, ukoliko pošaljemo GET zahtev ka ruti `http://localhost:3000/api/users/5eb2c28344c5e14d11dd32` iz razloga što prosleđeni identifikator nije korektan MongoDB identifikator (nema 24 heksadekadne cifre), naša aplikacija će vratiti odgovor sa statusnim kodom `500`, a ne statusnim kodom `404` kako je to bio slučaj u prethodnoj verziji aplikacije koju smo razvijali. Razlog za ovakvo ponašanje je taj što se prilikom poziva funkcije `findById` odgovarajućeg kontrolera prvo vrši validacija identifikatora koja u ovom slučaju rezultira izuzetkom koji se obrađuje u `catch` delu. Ako znamo da se za ovakav scenario generiše greška tipa `CastError` možemo popraviti ponašanje aplikacije dodavanjem sledeće provere neposredno pre slanja odgovora:
```javascript
if(err.name === 'CastError'){
  statusCode = 404;
}
```
Još neke greške koje se mogu javiti su`ValidationError` kada dokument koji se dodaje kolekciji ili nakon izmene ne ispunjava restrikcije definisane shemom, `MissingSchemaError` ukoliko se ne navede shema na nivou `model` funkcije, `MongooseError` kao generalna greška i druge.

Gore opisana podela zaduženja dovodi do novog sadržaja našeg fajla `users.js` u kojem su opisana zaduženja rutera. Sada fajl ima sledeći sadržaj:

```javascript
const usersController = require('../../controllers/usersController.js');
const express = require('express');
const router = express.Router();

router.get('/', usersController.getUsers);
router.get('/:id', usersController.getUserById);
router.post('/', usersController.addNewUser);
router.put('/:id', usersController.updateUserbyId);
router.delete('/:id', usersController.deleteUserbyId);

module.exports = router;
```

U opštem slučaju jednoj ruti se može pridružiti više kontrolera. Na primer, ukoliko je nakon ažuriranja informacija o korisniku potrebno izvršiti i neka ažuriranja statistika kontrolerom `userController.updateStatistics` to se na nivou rutera može zapisati sa  
```javascript 
router.put('/:id', [usersController.updateUserbyId, usersController.updateStatistics]);
```
Ipak, da bi sve ovo glatko funkcionisalo, na nivou prvog kontrolera se funkcijom  `next()` treba prepustiti izvršavanje sledećem kontroleru. 

Finalna promena koju ćemo izvršiti tiče se reorganizacije `app.js` fajla. Hteli bismo da razdvojimo funkcionalnosti koje se tiču samog rada servera tj. njegovog osluškivanja događaja, prihvatanja zahteva i generisanja odgovora, i same aplikacije tj. skupa njenih podržanih ruta i radnji koje one impliciraju, konekcija koje ona održava i slično.

Novi sadržaj fajla `app.js` je prikazan sledećim blokom koda.

```javascript
const express = require('express');
const mongoose = require('mongoose');
const { urlencoded, json } = require('body-parser');

const app = express();

app.use(json());
app.use(urlencoded({ extended: false }));

const databaseString = 'mongodb://localhost:27017/demo';
mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.once('open', function () {
  console.log('povezani na bazu');
});

mongoose.connection.on('error', (err) => {
  console.log('Greska: ', err);
});

const usersRoutes = require('./routes/api/users');
app.use('/api/users', usersRoutes);

app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan!');
  error.status = 405;
  next(error);
});

app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  res.status(statusCode).json({
    error: {
      message: err.message,
      status: statusCode,
      stack: err.stack,
    },
  });
});

module.exports = app;
```

Prvo što možemo da primetimo je da izvozimo aplikaciju na nivou modula tako da je server može koristiti. Druga dopuna tiče se promene funkcije kojom se reaguje na zahteve ka putanjama koje nisu podržane ruterom. Ova funkcija sada eksplicitno pozivom funkcije `Error` kreira objekat greške i prosleđuje je `next` pozivom funkciji koja je navedena odmah ispod, a koja objekat greške očekuje kao prvi argument.

U fajlu `server.js` ćemo sada na osnovu aplikacije kreirati server pozivom funkcije `createServer`. Ovako kreirani server se dalje može konfigurisati na osnovu okruženja u kojem se izvršava i minimalno menjati za različite aplikacije koje treba da izvršava. Sledeći blok prikazuje sadržaj `server.js` fajla.

```javascript
const http = require('http');
const app = require('./app');
const server = http.createServer(app);
const port = 3000;
server.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```

Aplikacije obično rade sa većim brojem rutera i njima pripadajućih kontrolera i modela. Ukoliko je potrebno iz jednog modela tj. njemu pripadajuće sheme referisati na neku drugu shemu, prilikom kreiranja sheme modela može se uz odgovarajuće svojstvo navesti `ref` podešavanje. Na primer, ukoliko je sledećom shemom 

```javascript
const Post = mongoose.model('posts', mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId, 
  title: String,
  content: String,
  topic: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    required: true
  }  
}));
```
predstavljen jedan post na blogu, fragmentom sheme

```javascript
author: {
  type: mongoose.Schema.Types.ObjectId,
  ref: 'users',
  required: true
}
```
se naglašava da će svojstvo *author* biti identifikator koji se nalazi u kolekciji  *users*. Nakon izvršavanja upita, da bi se dohvatile i vrednosti referisanih objekata, očekuje se poziv funkcije `populate`. U duhu prethodnog primera, ako se pretražuju svi postovi sa temom *'programming'*, pozivom funkcije `populate` će se pročitati podaci odgovarajućih autora koji se dalje mogu koristiti. Kod koji odgovara primeru je naveden ispod.  
```javascript
const post = await Post.find({topic: 'programming'}).populate('author');
console.log(post.author.username);
```
Ukoliko na nivou sheme postoji više referenci, pozivi funkcije `populate` se mogu ulančavati. Na primer, da nivou sheme bloga imamo i referencu na korisnika koji ima ulogu *editor*, odgovarajući fragment koda bi bio:
```javascript
const post = await Post.find({topic: 'programming'}).populate('author').populate('editor');
```
