const express = require('express');
const usersController = require('../../contollers/users');
const verifyToken = require('./../../utils/authentication');


const router = express.Router();

router.get('/', usersController.getAllUsers);
router.get('/:username', usersController.getUserByUsername);

router.post('/', verifyToken, usersController.addNewUser);
router.put('/', verifyToken, usersController.changeUserPassword);
router.delete('/:username', verifyToken, usersController.deleteUser);

module.exports = router;
