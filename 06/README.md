# 6. sedmica vežbi

## Node.js - uvodni primeri

- [Primeri sa časa](./primeri/)
- [Node.js tekst](./nodejs.md)
    - Ovo je preliminarna verzija teksta (od prošle godine). Kada budemo ažurirali skriptu, tada ćemo ažurirati i ovaj link.
    - Neki primeri iz teksta se malo razlikuju u odnosu na one koje smo radili na času
