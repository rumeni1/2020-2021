const url = require('url');

// console.log(url.URL == URL);

const testURL = new url.URL('http://testwebsite.com:3000/info/users.html?id=1000&status=active');

// stariji nacin, oznacen kao depricated u novijim Node.js verzijama 
// const testURL = url.parse('http://testwebsite.com:3000/info/users.html?id=1000&status=active');
// console.log(testURL);


// string reprezentacija URL-a
console.log(testURL.href);

// mrezno ime 
console.log(testURL.hostname);

// port 
console.log(testURL.port);

// putanja do trazenog fajla
console.log(testURL.pathname);

// parametri pretrage u tekstualnoj formi
console.log(testURL.search);

// parametri pretrage u formi niza 
console.log(testURL.searchParams);
testURL.searchParams.forEach((value, name) => {
    console.log(name, value);
});





