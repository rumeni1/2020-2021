const uuid = require('uuid');

// UUID - Universal Unique Identifier

// V1 vrednost se generise koriscenjem vremenske odrednice i MAC adrese masine
console.log('Timestamp uuid: ', uuid.v1());

// V4 vrednost se generise koriscenjem slucajnih brojeva
console.log('Random uuid: ', uuid.v4());

