const express = require('express');
const { urlencoded, json } = require('body-parser');
const validator = require('validator');
const users = require('../users/users');

const app = express();

app.use(json());
app.use(urlencoded({ extended: false }));

app.get('/api/users', (req, res) => {
  const allUsers = users.getAllUsers();
  res.status(200).json(allUsers);
});

app.get('/api/users/:username', (req, res) => {
  const username = req.params.username;

  if (username == undefined) {
    res.status(400).json();
  } else {
    const user = users.getUserByUsername(username);
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  }
});

app.post('/api/users', (req, res) => {
  const { username, email, password } = req.body;

  // skraceni zapis za username == undefined || email == undefined || password == undefined
  if (
    !username ||
    !email ||
    !password ||
    !validator.isEmail(email) ||
    !validator.isAlphanumeric(username)
  ) {
    res.status(400).json();
  } else {
    const isAdded = users.addNewUser(username, email, password);
    if (isAdded) {
      const user = users.getUserByUsername(username);
      res.status(201).json(user);
    } else {
      res.status(403).json();
    }
  }
});

app.put('/api/users', (req, res) => {
  const { username, oldPassword, newPassword } = req.body;

  if (!username || !oldPassword || !newPassword) {
    res.status(400).json();
  } else {
    const isChanged = users.changeUserPassword(
      username,
      oldPassword,
      newPassword
    );
    if (isChanged) {
      const user = users.getUserByUsername(username);
      res.status(200).json(user);
    } else {
      res.status(404).json();
    }
  }
});

app.delete('/api/users/:username', (req, res) => {
  const username = req.params.username;

  if (!username) {
    res.status(400).json();
  } else {
    const isDeleted = users.deleteUser(username);
    if (isDeleted) {
      res.status(200).json();
    } else {
      res.status(404).json();
    }
  }
});

module.exports = app;
