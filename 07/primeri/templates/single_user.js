const express = require('express');
const users = require('../users/users');

const app = express();
const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/api/users/:id', (req, res) => {
  const user = users.getUserById(req.params.id);
  res.render('user.ejs', { user: user });
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
